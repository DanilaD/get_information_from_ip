<?php

  class workIP {
    
    // path to Geo data
    var $file = [
          
          'file_search' => './geoip/SxGeo.php',
      
      ];
    
    // path to Db
      var $data = './geoip/SxGeoCity.dat';
      
    // Google API KEY
      var $googleApi = 'AIzaSyCAp4za6IqZqCcapv06UQiBL-kqcfiQTrg';
    
    // message
      var $msg =
              [
                '0' => ['status' => 'warning', 'text' => 'we don\'t have information, please check IP and try once again'],
                '1' => ['status' => 'success', 'text' => 'we have location'],
                '2' => ['status' => 'danger', 'text' => 'we have critical error, sorry'],
              ];
    
    // check and get db
      private function getDataDb() {

        return (is_file($this->data)) ? $this->data : self::Error404();

      }
    
    // prepare information to display
      private function getDataCity($ip) {
        
        // get Db from file
          self::GetFile();
    
        // check Db
          $data = self::getDataDb();
          
          if (!isset($data['status'])) {

            // work with Db
              $SxGeo = new SxGeo($data, SXGEO_BATCH | SXGEO_MEMORY);

            // information
              $data = $SxGeo->getCityFull($ip); 

          } 
          
          return $data;
          
      }
    
    // to display data      
      private function recurciveArray($array){

        foreach ($array as $key => $value) {

          if (is_array($value)) { 

            echo '<h2>'.$key.'</h2>';

            self::recurciveArray($value);

          } else {

           echo '<li><b>'.$key.'</b> = '.$value.'</li>';

          }
        }

        return $this->msg['1'];

      }
    
    // check and get file
      private function GetFile() {

        foreach ($this->file AS $key => $value) {

          $res[$key] = file_exists($value) ? include($value) : self::Error404();

        }

        return $res;

      }
    
    // get remote IP
      public function getInformationIP() {

        return  getenv('HTTP_CLIENT_IP')?:

                getenv('HTTP_X_FORWARDED_FOR')?:

                getenv('HTTP_X_FORWARDED')?:

                getenv('HTTP_FORWARDED_FOR')?:

                getenv('HTTP_FORWARDED')?:

                getenv('REMOTE_ADDR');

      }

    // for 404 error
      private function Error404() {

        return $this->msg['2'];      
        
      }
      
    // for work
      public function displayData($ip, $display = TRUE) {
      
      // get information
        $array = self::getDataCity($ip);
      
      // if we don't have information about IP
        if (empty($array)) return $this->msg['0'];
        
      // if critical error
        if (isset($array['status'])) return $this->msg['2'];
      
        if ($display == TRUE) {
          
          $result = $this->msg['1'];
          
          $result['data'] = $array;
              
        } else {
          
          $result = self::recurciveArray($array);
          
        }
      
      return $result;
        
    }
  
  }