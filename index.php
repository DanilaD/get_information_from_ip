<?PHP
    
  // global calss
    include './core/main.php';  
  
  // main class for work
    $core = NEW workIP();
    
  // get input  
    $ip = filter_input(INPUT_GET, 'ip', FILTER_VALIDATE_IP);
    
  // check input and get IP
    (empty($ip)) ? $ip = $core->getInformationIP() : '';
   
  // get data from Db
    $msg = $core->displayData($ip);
		
	// google API key
		$googleApi = $core->googleApi;
    
  // get templates
  include './index.html';