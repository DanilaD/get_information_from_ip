online service http://4x4forum.by/ip/

simple to work

1. copy all files

2. if you want to change name of dir, you must change this at './core/main.php' in line 8,13

3. you need to put on cron update data base of IP-addresses once a day: './geoip/Update.php'

4. you need to add you google api key at './core/main.php' in line 16

all information about service IP you can find here - https://sypexgeo.net/


- - -

structure

./core/main.php - my file with class to display location by IP
index.php - main file 
index.html - templates

- - -
./geoip/SxGeo.dat - Db
./geoip/SxGeo.php - Main file for find information
./geoip/SxGeo.upd - file with information of last update
./geoip/SxGeoCity.dat - Db
./geoip/Update - update Db from official web site


